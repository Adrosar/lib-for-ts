
## Wstęp

Biblioteka dla projektów opartych na Webpack + Babel + TypeScript. Zawiera pomocnicze klasy, funkcje i obiekty.

*PS. Jest w trakcie rozwoju :)*


## Instalacja środowiska

Aby zainstalować środowisko deweloperskie przejdź do projektu:

	https://bitbucket.org/Adrosar/webpack2-starterkit-full

i wykonaj wszystkie kroki z akapitu **INSTALACJA**.


## Struktura projektu, zadania i testy

Projekt bazuje na paczce [webpack2-starterkit-full](https://bitbucket.org/Adrosar/webpack2-starterkit-full), więc **struktura projektu, zadania i testy** jednostkowe są takie same :)