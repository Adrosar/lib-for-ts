
export type EventHandle = {
    (item: EventTarget, event: string, data: any): void
}

export type EventHandlesMap = {
    [name: string]: Array<EventHandle>
}

export type EventTarget = {
    _eventHandlesMap: EventHandlesMap
}

export type EventTargetList = Array<EventTarget>

export type EachFunction = {
    (target: EventTarget, name: string, handle: EventHandle): void
}

export class EventManager {

    private _eventTargetList: EventTargetList

    constructor() {
        this._eventTargetList = [];
    }

    private _removeItemfromArray(itemToRemove: any, array_: Array<any>): void {
        let index: number = array_.indexOf(itemToRemove);

        if (index > -1) {
            array_.splice(index, 1);
        }
    }

    public getEventTargetList(): EventTargetList {
        return this._eventTargetList;
    }

    public each(fn: Function): void {
        for (let i = 0; i < this._eventTargetList.length; i++) {
            let eventHandlesMap = this._eventTargetList[i]['_eventHandlesMap'];

            for (let event in eventHandlesMap) {
                if (eventHandlesMap.hasOwnProperty(event)) {
                    let handles = eventHandlesMap[event];

                    for (let j = 0; j < handles.length; j++) {
                        let handle = handles[j];

                        if (typeof fn === "function") {
                            fn(this._eventTargetList[i], event, handle);
                        }
                    }
                }
            }
        }
    }

    public bind(item: EventTarget, event: string, handle: Event): void {

        if (typeof handle === "function") {

            if (typeof item._eventHandlesMap !== "object") {
                item._eventHandlesMap = {};
            }

            if (!(item._eventHandlesMap[event] instanceof Array)) {
                item._eventHandlesMap[event] = [];
            }

            item._eventHandlesMap[event].push(handle);
            this._eventTargetList.push(item);
        }
    }


    public unbind(item: EventTarget, event: string, handleToRemove: EventHandle): void {
        if (typeof item._eventHandlesMap === "object") {
            if (item._eventHandlesMap[event] instanceof Array) {
                let handles: Array<EventHandle> = item._eventHandlesMap[event];

                for (let i = 0; i < handles.length; i++) {
                    let handle: EventHandle = handles[i];

                    if (handle === handleToRemove) {
                        this._removeItemfromArray(handleToRemove, handles);
                    }
                }
            }
        }
    }

    public dispatch(item: EventTarget, event: string, data?: any): void {
        if (typeof item._eventHandlesMap === "object") {
            if (item._eventHandlesMap[event] instanceof Array) {

                let handles: Array<EventHandle> = item._eventHandlesMap[event];

                for (let i = 0; i < handles.length; i++) {
                    let handle: EventHandle = handles[i];

                    if (typeof handle === "function") {
                        handle(item, event, data);
                    }
                }
            }
        }
    }

}
