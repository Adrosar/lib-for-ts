
require("./index.html");
require("./style.css");

import { EventManager } from "./event";

const LIB_NAMEs = ["__lib"];
const LIB: any = {
    "EventManager": EventManager
}

for (let i = 0; i < LIB_NAMEs.length; i++) {
    let name: string = LIB_NAMEs[i];

    if (typeof (<any>window)[name] !== "object") {
        (<any>window)[name] = LIB;
    }
}
