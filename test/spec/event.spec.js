(function () {

    if (typeof window.__lib !== "object") {
        throw new Error();
    }

    var lib = window.__lib;

    if (typeof lib.EventManager !== "function") {
        throw new Error();
    }

    var em = new lib.EventManager();

    if (typeof em.bind !== "function") {
        throw new Error();
    }

    if (typeof em.dispatch !== "function") {
        throw new Error();
    }

    if (typeof em.getEventTargetList !== "function") {
        throw new Error();
    }

    var eventTarget = {};

    var handle = function (target, event, data) {
        if (typeof target !== "object") {
            throw new Error();
        }

        if (typeof event !== "string") {
            throw new Error();
        }

        if (data !== 12345) {
            throw new Error();
        }
    }

    em.bind(eventTarget, "demo", handle);
    em.dispatch(eventTarget, "demo", 12345);

    if (typeof em.getEventTargetList() !== "object") {
        throw new Error();
    }

    if (em.getEventTargetList().length !== 1) {
        throw new Error();
    }


    /// EventManager.each()
    if (typeof em.each !== "function") {
        throw new Error();
    }

    em.each(function (target, event, handle) {

        if (typeof target !== "object") {
            throw new Error();
        }

        if (event !== "demo") {
            throw new Error();
        }

        if (typeof handle !== "function") {
            throw new Error();
        }

    });


    /// EventManager.unbind()
    if (typeof em.unbind !== "function") {
        throw new Error();
    }

    em.unbind(eventTarget, "demo", handle);
    em.each(function (target, event, handle) {
        throw new Error();
    });

})();